var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var cors = require('cors')
var session = require('express-session')

/*******************************
*          GRAPHQL             *
********************************/
const { graphqlExpress, graphiqlExpress } = require('apollo-server-express')
const { makeExecutableSchema } = require('graphql-tools')
const resolvers = require('./graphql/resolvers/')
const schemaList = require('./graphql/typeDefs/')
const schema = makeExecutableSchema({
  typeDefs: schemaList,
  resolvers
})

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(cors({
  origin:[
    'http://localhost:3000',
    'http://localhost:4000',
  ],
  credentials: true
}))
app.use(session({
  secret: "secret",
  resave: false,
  saveUninitialized: true,
  cookie: { path: '/' ,secure: false, maxAge: 86000000 }
}))
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

// graphql
app.use(
  '/graphql',
  // bodyParser.json(),
  graphqlExpress(req => {
    return {
      schema: schema,
      context: {
        body: req.body,
        session: req.session
      }
    }
  })
)

app.use(
  '/graphqlTypeB',
  // bodyParser.json(),
  graphqlExpress(req => {
    return {
      schema: schema,
      context: {
        body: req.body
      }
    }
  })
)

var port = process.env.PORT || '3000';
const subEndpointDev = `ws://localhost:${port}/subscriptions`
const subEndpointHeroku = `wss://fast-beach-82436.herokuapp.com/subscriptions`

app.use('/graphiql', graphiqlExpress({
  endpointURL: '/graphql',
  subscriptionsEndpoint: subEndpointHeroku
}))

// app.use('/graphiqlTypeB', graphiqlExpress({
//   endpointURL: '/graphqlTypeB',
//   subscriptionsEndpoint: `ws://localhost:${port}/subscriptions`
// }))

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
