const { PubSub, withFilter } = require('graphql-subscriptions');
const Dataloader = require('dataloader')

const pubsub = new PubSub();
const findPeople = new Dataloader(names => {
  return batchUser('name', ...names)
})

let messageList = [
  { text: 'testing', channelId: 1 },
  { text: 'wtf man no2', channelId:1 }
]

const subResolver = {
  Query: {
    getUserStatus: (root, args, context) => {
      if (context.session.user) {
        return {
          status: true,
          username: context.session.user.username
        }
      } else {
        return {
          status: false,
          username: ''
        }
      }
    },
    messageGet: (root, args, context) => {
      console.log(messageList);
      console.log(context.session);
      if (!context.session.identification) {
        context.session.identification = "user"
      }

      // context.session.save(err => {
      //   console.log(err);
      // })
      return messageList
    },
    getPerson: (root, args, context) => {
      console.log(context.session);
      if (context.session.person) {
        context.session.person++
      } else {
        context.session.person = 1
      }
      console.log('returning peoples');
      return people
    },
    getPersonDataLoader: (root, args, context) => {
      return people
    }
  },
  Mutation: {
    login: (root, args, context) => {
      if (!context.session.user) {
        context.session.user = {
          username: args.username
        }
      }
      console.log(context.session.id);
      let token = args.password[1] + 'sometokenabcdefg'
      return {
        token: token
      }
    },
    logout: (root, args, context) => {
      if (context.session.user) {
        if (context.session.user.username == args.username) {
          context.session.destroy(err => {
            console.log(err);

          })
          return {
            message: 'logout successful'
          }
        } else {
          return {
            message: 'wrong user to logout'
          }
        }
      } else {
        return {
          message: 'not logged in'
        }
      }
    },
    messageAdd: (root, args, context) => {
      if (context.session.message) {
        context.session.message++
      } else {
        context.session.message = 1
      }
      console.log(context.session);
      let message = {
        text: args.message.text,
        channelId: args.message.channelId
      }
      messageList.push(message)
      // send payload for subs
      pubsub.publish('messageSub', {
        messageSub: message
      })
      // console.log(messageList);
      return message
    }
  },
  Subscription: {
    messageSub: {
      resolve: (payload) => {
        // Process data as required
        return {
          text: payload.messageSub.text,
          channelId: payload.messageSub.channelId
        }
      },
      subscribe: withFilter(
        // receive payload
        () => pubsub.asyncIterator('messageSub'),
        // filter payload
        (payload, args) => {
          // console.log(payload.messageSub.channelId);
          // console.log(args.message.channelId);
          if (payload.messageSub.channelId == args.message.channelId) {
            return true
          } else {
            return false
          }
        }
      )
    }
  },
  Person: {
    name: (root) => {
      return root.name.toUpperCase()
    },
    bestFriend: (root) => {
      console.log('finding best friend ' + root.best_friend);
      let filter = people.filter(person => person.name == root.best_friend)
      return filter[0]
    }
  },
  PersonDataLoader: {
    name: (root) => {
      return root.name.toUpperCase()
    },
    bestFriend: ({ best_friend }) => {
      return findPeople.load(best_friend)
    }
  }
}

function batchUser(field, ...values) {
  console.log(`finding people with ${field} === ${values.join(', ')}`)
  return Promise.resolve(
    values.map(value => {
      let data = people.filter(person =>
        person.name == value
      )
      return data[0]
    })
  )
}

const people = [
  {
    name: 'George',
    age: 17,
    gender: 'MALE',
    height: 72,
    best_friend: 'Alexander',
  },
  {
    name: 'Jill',
    age: 19,
    gender: 'FEMALE',
    height: 65,
    best_friend: 'Alexander',
  },
  {
    name: 'Alexander',
    age: 32,
    gender: 'MALE',
    height: 68,
    best_friend: 'George',
  },
  {
    name: 'Dave',
    age: 19,
    gender: 'MALE',
    height: 58,
    best_friend: 'George',
  }
];

module.exports = subResolver;
